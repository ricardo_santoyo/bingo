import React, { Component } from "react";

import "./sass/main.css";
import Bingo from "./components/Bingo";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams
} from "react-router-dom";

class App extends Component {

  render() {
    return (
      <Router>
        <div className="app-container">
          <Switch>
            <Route path="/contact">
              this is contact
            </Route>
          </Switch>
          <Bingo/>
        </div>  
      </Router>
      
    );
  }
}

export default App;



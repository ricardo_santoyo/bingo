import React, { useState, useEffect } from "react";
import { useRouteMatch } from "react-router-dom";
import { css } from "emotion";
import classNames from "classnames";

const Bingo = () => {
  const [board, setBoard] = useState([]);
  const [sorted, setSorted] = useState([]);
  const [indexGame, setIndexGame] = useState(0);
  const [numbersInGame, setNumbersInGame] = useState([]);
  const [start, setStart] = useState(false);
  let match = useRouteMatch("/:quantity");
  useEffect(() => {
    let bingoSize = 75;
    if (match) {
      if (!isNaN(match.params.quantity)) {
        bingoSize = Number(match.params.quantity);
      }
    }

    let to100 = [...Array(bingoSize + 1).keys()].slice(1);

    let temp = [];
    to100.map((item, index) => temp.push({ val: item, selected: false }));
    setBoard(...board, temp);

    var shuffle = require("shuffle-array"),
      toSort = shuffle(to100);
    setSorted(...sorted, toSort);

    setIndexGame(0);
  }, []);

  const handleClick = (e) => {
    setStart(true)
    let index = indexGame;
    setNumbersInGame([...numbersInGame, sorted[index]]);
    index++;
    setIndexGame(index);

    setBoard(
      board.map((item) =>
        item.val === sorted[indexGame] ? { ...item, selected: true } : item
      )
    );
  };

  const boardRows = css`
    grid-template-rows: repeat(${Math.ceil(board.length / 5)}, 1fr);
  `;

  return (
    <div className="bingo">
      <div className="bingo__title">
        <li className="board__cell board__cell-title">B</li>
        <li className="board__cell board__cell-title">I</li>
        <li className="board__cell board__cell-title">N</li>
        <li className="board__cell board__cell-title">G</li>
        <li className="board__cell board__cell-title">O</li>
      </div>
      <div className="bingo__container">
        <ul className={classNames("board", boardRows)}>
          {board.map((item, index) => (
            <li
              className={
                item.selected ? "board__cell board__cell-on" : "board__cell"
              }
            >
              {item.val}
            </li>
          ))}
        </ul>
        <div className="display">
          <div
            className={
              start ? "display__number" : "display__number display__number-off"
            }
          >
            <span className="display__ball">{sorted[indexGame-1]}</span>
          </div>
          <button onClick={handleClick} className="display__button">
            next
          </button>
        </div>
      </div>
    </div>
  );
};

export default Bingo;
